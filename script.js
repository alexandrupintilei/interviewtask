(function(globals) {
    globals.fakeAjax = function(url, cb) {
        var fake_responses = {
            request1: 'The first text',
            request2: 'The middle text',
            request3: 'The last text'
        };

        var randomDelay = (Math.round(Math.random() * 1e4) % 8000) + 1000;

        console.log('Requesting: ' + url);

        setTimeout(function() {
            cb(fake_responses[url]);
        }, randomDelay);
    };

    globals.output = function(text) {
        console.log(text);
    };
})(window);

function printInOrder(requests) {
    for (let i in requests) {
        Promise.all(requests.slice(0, i + 1)).then(responses => {
            output(responses[i]);
        });
    }
}

const ajax = function(url) {
    return new Promise(res => fakeAjax(url, res));
};

printInOrder([ajax('request1'), ajax('request2'), ajax('request3')]);